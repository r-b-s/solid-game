﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GuessingGame.DataAccess;
using GuessingGame.Model;
using GuessingGame.View;

namespace GuessingGame.Services
{
    public class GameLogic : ILogic,IRandom
    {
        private ISettingsGetter _settingsGetter { get; set; }

        private int counter { get; set; }
        private int secret { get; set; }

        public GameLogic(ISettingsGetter settingsGetter)
        {
            _settingsGetter = settingsGetter;
            counter = 1;
            secret = GetRandom(_settingsGetter.GetNumberRange());
        }

        public LogicResponse Compare (int d1)
        {
            counter++;
            string msg="";
            bool IsEnd = false;
            if (d1 == secret) { msg = "win"; IsEnd = true; }
            else
            {
                if (counter > _settingsGetter.GetAttemptsCount()) { msg = "lose"; IsEnd = true; }
                else if (secret > d1) { msg = ">"; }
                else msg = "<";
            }
            return new LogicResponse() { Output=msg,Counter=counter.ToString(),IsEnd=IsEnd};
        }

        public int GetRandom(int range)
        {
            var random = new Random();
            return  random.Next(range);
        }



    }
}
