﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GuessingGame.Services
{
    public interface IRandom
    {
        public int  GetRandom(int range);
    }
}
