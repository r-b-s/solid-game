﻿using GuessingGame.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GuessingGame.Services
{
    public interface ILogic
    {
        LogicResponse Compare(int d1);
    }
}
