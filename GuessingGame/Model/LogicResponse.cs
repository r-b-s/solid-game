﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GuessingGame.Model
{
    public class LogicResponse
    {
        public string Output;
        public string Counter;
        public bool IsEnd;
    }
}
