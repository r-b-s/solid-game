﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GuessingGame.Config
{
    public abstract class Settings
    {
        abstract public Dictionary<String,String?> GetSettings();
    }
}
