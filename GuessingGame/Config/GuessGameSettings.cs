﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GuessingGame.Config
{
    public class GuessGameSettings : GameSettings
    {
        public GuessGameSettings()
        {
            AttemptsCount = int.Parse(GetSettings()["AttemptsCount"]?? "0");
            NumberRange = int.Parse(GetSettings()["NumberRange"]??"0"); 
        }

        public int AttemptsCount { get; private set; }
        public int NumberRange { get; private set; }



    }
}
