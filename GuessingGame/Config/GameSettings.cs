﻿using Microsoft.Extensions.Configuration;
using System.Configuration;

namespace GuessingGame.Config
{
    public class GameSettings : Settings
    {
        public override Dictionary<String,String?> GetSettings()
        {
            var config = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json", false)
                .Build();
            return config.GetChildren().ToDictionary(x => x.Key, x => x.Value);
        }
    }
}
