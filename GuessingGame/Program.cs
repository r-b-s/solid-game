﻿using GuessingGame.Config;
using GuessingGame.DataAccess;
using GuessingGame.Services;
using GuessingGame.View;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

using IHost host = Host.CreateDefaultBuilder(args)
    .ConfigureServices((_, services) =>
        services.AddSingleton<IProcess, GameProcess>()
        .AddSingleton<ILogic, GameLogic>()
        .AddSingleton<ISettingsGetter, SettingsGetter>()
        .AddTransient<GuessGameSettings>()
        //.AddTransient<GameSettings>()
        )
        
.Build();

using IServiceScope serviceScope = host.Services.CreateScope();
IServiceProvider provider = serviceScope.ServiceProvider;

var process = provider.GetRequiredService<IProcess>();
process.Start();


await host.RunAsync();