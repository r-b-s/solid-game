﻿using GuessingGame.DataAccess;
using GuessingGame.Model;
using GuessingGame.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace GuessingGame.View
{
    public class GameProcess : IProcess
    {
        private ISettingsGetter _settingsGetter { get; set; }
        private ILogic _gameLogic { get; set; }

        public GameProcess(ISettingsGetter settingsGetter,ILogic logic)
        {
            _settingsGetter = settingsGetter;
            _gameLogic = logic;
        }
        public void Start()
        {

            Console.WriteLine($"Число от 0 до : {_settingsGetter.GetNumberRange()}");
            Console.WriteLine($"Количество возможных попыток: {_settingsGetter.GetAttemptsCount()}");
            Console.WriteLine($"Попытка №1");
            LogicResponse echo=null;
            do
            {
                if (!(echo is null)) {
                    Console.WriteLine($"загаданное число {echo.Output} вашего");
                    Console.WriteLine($"Попытка №{echo.Counter}"); 
                }
                var number = Convert.ToInt32(Console.ReadLine());
                echo=_gameLogic.Compare(number);
            }
            while(!echo.IsEnd);

            if (echo.Output=="win")
            Console.WriteLine("Победа!");
            else Console.WriteLine("Проигрыш :(");

   
            
        }
    }
}
