﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GuessingGame.DataAccess
{
    public interface ISettingsGetter
    {
        int GetAttemptsCount();
        int GetNumberRange();
        bool IsGuessGameSettings();
    }
}
