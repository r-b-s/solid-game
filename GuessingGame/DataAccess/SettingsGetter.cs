﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime;
using System.Text;
using System.Threading.Tasks;
using GuessingGame.Config;

namespace GuessingGame.DataAccess
{
    public class SettingsGetter : ISettingsGetter
    {
        private GameSettings _settings;
        public SettingsGetter(GuessGameSettings settings)
        {
            settings.GetSettings();
            _settings = settings;
        }

        public int GetAttemptsCount()
        {
            return int.Parse(_settings.GetSettings()["AttemptsCount"] ?? "0");
        }

        public int GetNumberRange()
        {
            return int.Parse(_settings.GetSettings()["NumberRange"] ?? "0");
        }

        public bool IsGuessGameSettings()
        {
            return _settings is GuessGameSettings;
        }
    }
}
