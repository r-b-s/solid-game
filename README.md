# GuessingGame

Игра "Угадай число".
Программа рандомно генерирует число, пользователь должен угадать это число. При каждом вводе числа программа пишет больше или меньше отгадываемого. Кол-во попыток отгадывания и диапазон чисел должен задаваться из настроек.

Использование SOLID:

1. Single Responsibility Principle - Каждый класс отвечает только за одно действие (Program - запускает программу, GameLogic - логика , GameProcess - ввод/вывод, LogicResponse - сообщение между модулями, GameSettings - настройки )
2. Open/Closed Principle -  потомок GuessGameSettings расширяет функционал базового GameSettings не меняя его 
3. Liskov Substitution Principle - в SettingsGetter в поле типа GameSettings внедряется потомок GuessGameSettings (в Program можно раскомментарить //.AddTransient<GameSettings>() - функционал не изменится)
4. Interface Segregation Principle - класс GameLogic реализует множество интрфейсов (IRandom,ILogic,...)
5. Dependency Inversion Principle - используется внедрение зависимостей в классе Program 
